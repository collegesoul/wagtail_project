#Provision resource for ECR
resource "aws_ecr_repository" "wagtail" {
  name = var.repo_name
  image_tag_mutability = "MUTABLE"
}

#outputs the ECR repository url
output "ecr_uri" {
  value = aws_ecr_repository.wagtail.repository_url
}

#provision resource for apprunner service for the application
resource "aws_apprunner_service" "wagtail_runner" {
  depends_on = [aws_ecr_repository.wagtail]

  service_name = var.service_name

  source_configuration {

    image_repository {
      image_configuration {
        port = var.port
      }

      image_identifier      = "${aws_ecr_repository.wagtail.repository_url}:${var.image_tag}"
      image_repository_type = "ECR"
    }

    authentication_configuration {
      access_role_arn = var.access_role
    }
  }

  tags = {
    name = "${var.service_name}-service"
  }
}

