FROM python:3.10-slim-buster

EXPOSE 8080

#Create group and user
RUN groupadd -r wagtails
RUN useradd -r -g wagtails wagtails

#Install dependencies
RUN apt-get update --yes --quiet && apt-get install --yes --quiet --no-install-recommends \
    nano \
    build-essential \
    gettext \
    libpq-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
    python3-dev \
    libpq-dev \
    gcc \
    musl-dev \
 && rm -rf /var/lib/apt/lists/*
RUN pip install --upgrade pip

#Copy requirements from project and install
COPY ./requirements.txt .
RUN pip install -r requirements.txt

#create a wagtail site template
#copy existing wagtail template from project and change directory to mysite
RUN wagtail start mysite
COPY wagtails/mysite mysite
WORKDIR /mysite

#Make wagtail user own the project folder
RUN chown -R wagtails:wagtails /mysite

#copy shell scripts from project and make script executable
COPY wait-for-it.sh wait-for-it.sh
COPY entrypoint.sh entrypoint.sh
RUN chmod +x wait-for-it.sh
RUN chmod +x entrypoint.sh

USER wagtails

