variable "region"{
  type = string
  default = "eu-west-1"
}

variable "access_key"{
  type = string
  sensitive = true
}

variable "secret_key"{
  type = string
  sensitive = true
}

variable "session_token"{
  type = string
  sensitive = true
}

variable "repo_name"{
  type = string
  default = "tech_space"
  description = "The name of the repository to store docker image"
}

variable "service_name" {
  type = string
  default = "tech_space_runner"
  description = "The name of the app runner service"
}

variable "image_tag" {
  type = string
  default = "latest"
  description = "The tag for your image"
}

variable "port"{
  type = string
  default = "8080"
}

variable "access_role" {
  type = string
}

