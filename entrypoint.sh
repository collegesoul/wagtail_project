#!/usr/bin/env bash

# Wait for the PostgreSQL database to be available
./wait-for-it.sh db:5432 --timeout=30

# Apply database migrations
python manage.py migrate

# Create a superuser
source .env
DJANGO_SUPERUSER_PASSWORD=$PASSWORD python manage.py createsuperuser --noinput --username=$USERNAME  --email=$EMAIL

python manage.py runserver 0.0.0.0:8080

# Run the provided command
exec "$@"
