# Deploying Wagtail CMS on AWS Apprunner

This project containerizes the wagtail cms in docker and uses docker-compose
for connecting to db. There's a terraform script to automate creation of infrastructure
for the cms